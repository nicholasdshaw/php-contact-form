<!DOCTYPE html>

<html lang=&quot;en-US&quot;>

<head>

    <meta charset=&quot;utf-8&quot;>

</head>

<body>

<h2>Contact Inquiry</h2>

<div>

    <h5>Name: {{$contact['name']}}</h5>

    <h5>Email: {{$contact['email']}}</h5>

    @if($contact['phone'])

    <h5>Phone: {{$contact['phone']}}</h5>

    @endif

    <br>

    <h5>Message:</h5>

    <p>{{$contact['message']}}</p>

</div>

</body>

</html>

