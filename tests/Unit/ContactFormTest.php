<?php

namespace Tests\Unit;

use App\Mail\ContactEmail;
use App\Models\Message;
use Tests\TestCase;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactFormTest extends TestCase
{
    use DatabaseMigrations;


    protected $fake;


    public function __construct()
    {
        parent::__construct();

        $this->fake = Faker::create();

    }

    /**
     * Form submissions succeed when a post request contains valid data
     *
     * @return void
     */

    public function test_form_submission_succeeds_with_valid_data()
    {

        $data = array_merge([
            'name' => $this->fake->name,
            'email' => $this->fake->email,
            'phone' => $this->fake->phoneNumber,
            'message' => $this->fake->paragraph
        ]);

        $this->followingRedirects()
            ->post('/', $data)
            ->assertStatus(200)
            ->assertSee('Your message has been sent!');

    }

    /**
     * Form submissions succeed when a post request contains valid data, excluding phone number
     *
     * @return void
     */

    public function test_form_submission_succeeds_with_valid_data_excluding_phone_number()
    {

        $data = array_merge([
            'name' => $this->fake->name,
            'email' => $this->fake->email,
            'message' => $this->fake->paragraph
        ]);

        $this->followingRedirects()
            ->post('/', $data)
            ->assertStatus(200);
    }

    /**
     * Form submissions fails when a post request contains invalid data
     *
     * @return void
     */

    public function test_form_submission_fails_with_null_data()
    {

        $data = array_merge([
            'name' => null,
            'email' => null,
            'phone' => null,
            'message' => null
        ]);

        $this->followingRedirects()
            ->post('/', $data)
            ->assertSee('ERRORS')
            ->assertSee('The name field is required.')
            ->assertSee('The email field is required.')
            ->assertSee('The message field is required.');
    }

    /**
     * Form submissions fails when a post request contains an invalid email address
     *
     * @return void
     */

    public function test_form_submission_fails_with_invalid_email_address()
    {

        $data = array_merge([
            'name' => $this->fake->name,
            'email' => $this->fake->text,
            'message' => $this->fake->paragraph
        ]);

        $this->followingRedirects()
            ->post('/', $data)
            ->assertSee('ERRORS')
            ->assertSee('The email must be a valid email address.');
    }

    /**
     * Form submissions get stored in database submitting valid data
     *
     * @return void
     */

    public function test_form_gets_stored_when_submitting_valid_data()
    {

        $data = array_merge([
            'name' => $this->fake->name,
            'email' => $this->fake->email,
            'phone' => $this->fake->phoneNumber,
            'message' => $this->fake->paragraph
        ]);

        $this->followingRedirects()
            ->post('/', $data)
            ->assertStatus(200);

        $this->assertDatabaseHas('messages', [
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'message' => $data['message']
        ]);
    }

    public function test_form_submission_sends_email()
    {

        $data = array_merge([
            'name' => $this->fake->name,
            'email' => $this->fake->email,
            'phone' => $this->fake->phoneNumber,
            'message' => $this->fake->paragraph,
        ]);

        Mail::fake();

        $this->followingRedirects()
            ->post('/', $data)
            ->assertStatus(200);

        // Assert if email was sent

        Mail::assertSent(ContactEmail::class, function ($mail) use ($data) {

            return $mail->hasTo(config('mail.from.address'));

        });

    }

}
