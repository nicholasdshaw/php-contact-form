<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactFormRequest;
use App\Mail\ContactEmail;
use App\Models\Message;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{

    /**
     * Send message
     *
     * @param $request
     * @return array
     */

    public function send(ContactFormRequest $request)
    {

        // Store message in Database

        $message = Message::create($request->input());


        // Send Email

        Mail::to(config('mail.from.address'))->send(new ContactEmail($message));


        // Update message status

        $message->status = 'sent';

        $message->update();


        // Set success message

        flash('Your message has been sent!')->success();


        // Redirect

        return redirect(url()->previous() . '#contact');

    }
}
