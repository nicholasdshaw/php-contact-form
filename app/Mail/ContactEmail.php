<?php

namespace App\Mail;

use App\Models\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $contact;

    /**
     * Create a new message instance.
     *
     * @param $message
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->contact = $message;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this
            ->to(config('mail.from.address'), config('mail.from.name'))
            ->from($this->contact->email, $this->contact->name)
            ->subject('Contact Inquiry')
            ->view('emails.contact');

    }
}
